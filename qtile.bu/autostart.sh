#!/bin/bash
#

xrandr --output DP-1 --off --output DP-2 --mode 1920x1080 --pos 0x367 --rotate normal --output DP-3 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI-1 --mode 1920x1080 --pos 1920x1080 --rotate normal

#wlr-randr --output DP-3 --pos 2070,1570 --output DP-2 --pos 3990,970 --scale 1.30 --output HDMI-A-1 --pos 3990,2070 --scale 1.30

exec ~/.config/hypr/scripts/suspend.sh &
swww query || swww init & 
flatpak run org.jellyfin.JellyfinServer &

/usr/libexec/polkit-gnome-autentication-agent-1 &
