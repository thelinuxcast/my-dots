## Copyright (C) 2020-2022 Aditya Shakya <adi1090x@gmail.com>
## Everyone is permitted to copy and distribute copies of this file under GNU-GPL3
;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

;; Global WM Settings

[global/wm]
margin-bottom = 0
margin-top = 0

;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

;; File Inclusion
; include an external file, like module file, etc.

include-file = ~/.config/polybar/system.ini
include-file = ~/.config/polybar/themes/colors/kiss.ini
include-file = ~/.config/polybar/themes/modules/kiss.ini

;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

[bar/mainbar3-i3]

monitor = DP-2
monitor-strict = false
override-redirect = true
bottom = false
fixed-center = false
width = 100%
height = 25
;offset-x = 40
;offset-y = 5

background = ${colors.background}
foreground = ${colors.foreground}

; Background gradient (vertical steps)
;   background-[0-9]+ = #aarrggbb
;background-0 =

radius = 0.0
line-size = 2

border-size = 0
;border-left-size = 25
;border-right-size = 25
border-top-size = 0
border-bottom-size = 0
border-color = ${colors.bg0}

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

;https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "JetBrainsMono Nerd Font:size=10;2"
font-1 = "FontAwesome:size=22;5"
font-2 = "JetBrainsMono Nerd Font:size=10;2"
font-3 = "JetBrainsMono Nerd Font:size=22;5"

modules-left = i3 sep window-title  
modules-center = 
modules-right = music weather uptime memory2 cpu2 date 

;separator = |

;dim-value = 1.0

tray-detached = false
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 2
tray-maxsize = 20
tray-scale = 1.0
tray-position = right
tray-background = ${colors.background}

#i3: Make the bar appear below windows
;wm-restack = i3


; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
double-click-left =
double-click-middle =
double-click-right =

; Requires polybar to be built with xcursor support (xcb-util-cursor)
; Possible values are:
; - default   : The default pointer as before, can also be an empty string (default)
; - pointer   : Typically in the form of a hand
; - ns-resize : Up and down arrows, can be used to indicate scrolling
cursor-click =
cursor-scroll =



[bar/mainbar-i3]
monitor = DP-3
monitor-fallback =
monitor-strict = false
override-redirect = false
bottom = false
fixed-center = true
width = 100%
height = 30
offset-x = 0%
offset-y = 0%

background = ${colors.BG}
foreground = ${colors.FG}

radius-top = 0.0
radius-bottom = 0.0
line-size = 4
line-color = ${colors.AC}

border-top-size = 0
border-color = ${colors.AC}
padding = 2

module-margin-left = 0
module-margin-right = 0

font-0 = "JetBrains Mono:size=10;3"
font-1 = "Iosevka Nerd Font:size=12;3"
font-2 = "Iosevka:style=bold:size=12;3"


# Default
modules-left = i3
modules-center = 
modules-right = volume sep battery sep network sep date

separator =

spacing = 0

dim-value = 1.0

wm-name = 

locale = 

tray-position = none
tray-detached = false
tray-maxsize = 16
tray-background = ${colors.BG}
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 0
tray-scale = 1.0

; wm-restack =
enable-ipc = true
cursor-click = 
cursor-scroll = 

;i3
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

[bar/mainbar2-i3]
monitor = HDMI-1
monitor-fallback =
monitor-strict = false
override-redirect = false
bottom = false
fixed-center = true
width = 100%
height = 30
offset-x = 0%
offset-y = 0%

background = ${colors.BG}
foreground = ${colors.FG}

radius-top = 0.0
radius-bottom = 0.0
line-size = 4
line-color = ${colors.AC}

border-top-size = 0
border-color = ${colors.AC}
padding = 2

module-margin-left = 0
module-margin-right = 0

font-0 = "JetBrains Mono:size=10;3"
font-1 = "Iosevka Nerd Font:size=12;3"
font-2 = "Iosevka:style=bold:size=12;3"


# Default
modules-left = i3
modules-center = 
modules-right = volume sep battery sep network sep date

separator =

spacing = 0

dim-value = 1.0

wm-name = 

locale = 

tray-position = none
tray-detached = false
tray-maxsize = 16
tray-background = ${colors.BG}
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 0
tray-scale = 1.0

; wm-restack =
enable-ipc = true
cursor-click = 
cursor-scroll = 

;i3
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev


;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

;; Application Settings

[settings]
; The throttle settings lets the eventloop swallow up til X events
; if they happen within Y millisecond after first event was received.
; This is done to prevent flood of update event.
;
; For example if 5 modules emit an update event at the same time, we really
; just care about the last one. But if we wait too long for events to swallow
; the bar would appear sluggish so we continue if timeout
; expires or limit is reached.
throttle-output = 5
throttle-output-for = 10

; Time in milliseconds that the input handler will wait between processing events
;throttle-input-for = 30

; Reload upon receiving XCB_RANDR_SCREEN_CHANGE_NOTIFY events
screenchange-reload = false

; Compositing operators
; @see: https://www.cairographics.org/manual/cairo-cairo-t.html#cairo-operator-t
compositing-background = source
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
;format-foreground = 
;format-background = 
;format-underline =
;format-overline =
;format-spacing =
;format-padding =
;format-margin =
;format-offset =

; Enables pseudo-transparency for the bar
; If set to true the bar can be transparent without a compositor.
pseudo-transparency = false

;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
;;	    __________  ______
;;	   / ____/ __ \/ ____/
;;	  / __/ / / / / /_    
;;	 / /___/ /_/ / __/    
;;	/_____/\____/_/       
;;
;; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
