call plug#begin('~/.config/nvim/autoload/plugged')
Plug 'preservim/nerdtree'
Plug 'farmergreg/vim-lastplace'
Plug 'airblade/vim-gitgutter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'plasticboy/vim-markdown'

Plug 'ap/vim-css-color'
Plug 'tpope/vim-surround'
"Plug 'dylanaraps/wal.vim'

call plug#end()
